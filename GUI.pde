import controlP5.*;

//GLOBAL VARIABLES
ControlP5 c5;
Slider[] sliders;

//METHODS
void initGUI() {
  c5 = new ControlP5(this);
  //SETUP
  c5.setColorBackground(color(0));
  c5.setColorForeground(color(120));
  c5.setColorActive(color(180));
  c5.setColorCaptionLabel(color(20));
  
  ControlGroup ctrl = c5.addGroup("menu",15,25,35);
  ctrl.setColorLabel(color(255));
  ctrl.close();
  
  sliders = new Slider[5];
  int si = 0;
  //SLIDERS variable    min value    max value   actual value    x    y    w    h
  sliders[si++] = c5.addSlider("scatter", 1, 15, scatter, 10, 30, 200, 10);
  sliders[si++] = c5.addSlider("decay", 0.95, 1, decay, 10, 50, 200, 10);
  sliders[si++] = c5.addSlider("nSamples", 5, 255, nSamples, 10, 70, 200, 10);
  sliders[si++] = c5.addSlider("pop", 50, 1000, pop, 10, 90, 200, 10);
  sliders[si++] = c5.addSlider("stigS", 0, 3, stigS, 10, 110, 200, 10);
  
    for (int i = 0; i < si; i++) {
    sliders[i].setGroup(ctrl);
    sliders[i].captionLabel().toUpperCase(true);
    sliders[i].captionLabel().style().padding(4,3,3,3);
    sliders[i].captionLabel().style().marginTop = -4;
    sliders[i].captionLabel().style().marginLeft = 0;
    sliders[i].captionLabel().style().marginRight = -14;
    sliders[i].captionLabel().setColorBackground(0x99ffffff);
  }
  
  //c5.setAutoDraw(false);
}

void drawGUI(){
  c5.show();
  c5.draw();
}

