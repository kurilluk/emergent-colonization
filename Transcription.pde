void ReadClusters(String path, int cell_size, Map map)
{  
  String txt_clusters[] = loadStrings(path);  
  if (txt_clusters == null) {
    println(path + " file is missing!");
    return;
  }
  // Get clusters from file
  ArrayList<Cluster> cluster_list = new ArrayList<Cluster>(txt_clusters.length);
  for (int i = 0; i< txt_clusters.length; i++) {
    String txt_cells[]= split (txt_clusters[i], " ");
    //println(txt_cells[0]+" a "+txt_cells[1]);

    //Get cluster cells
    ArrayList<Cell> cluster_cells = new ArrayList<Cell>(txt_cells.length/3);
    for (int j = 0; j < txt_cells.length-1; j += 3)
    {
      int x = Integer.parseInt(txt_cells[j]);
      int y = -(Integer.parseInt(txt_cells[j+1]));
      int z = Integer.parseInt(txt_cells[j+2]);
      //println(x +" "+ y +" " + z);

      Cell cell =  new Cell(x, y, z, cell_size, i);
      cluster_cells.add(cell);
      //Register cell in map
      map.AddCell(cell);
    }
    //Create cluster and store it into a list
    Cluster cluster = new Cluster(cluster_cells, i);
    cluster_list.add(cluster);
  }
  //println("cells:" +map.map.size());
  map.AddClusters(cluster_list);
} 
/////////////////////////////////////////////////////////////////////////////////////////////////////
void ReadTypology(String path, Map map)
{  
  String txt_typology[] = loadStrings(path);  
  if (txt_typology == null) {
    println(path + " file is missing!");
    return;
  }
  // Get list of typology from file
  ArrayList<Typology> typology_list = new ArrayList<Typology>(txt_typology.length);
  for (int i = 0; i< txt_typology.length; i++) {
    String txt_cells[]= split (txt_typology[i], " ");
    //println(txt_cells[0]+" a "+txt_cells[1]);

    //Get one typology cells
    ArrayList<Integer[]> cells = new ArrayList<Integer[]>(txt_cells.length/3);
    int base_x=0; int base_y=0;
    for (int j = 0; j < txt_cells.length-1; j += 3)
    {
      int x = Integer.parseInt(txt_cells[j]);
      int y = -(Integer.parseInt(txt_cells[j+1]));
      int z = Integer.parseInt(txt_cells[j+2]);
      //println(x +" "+ y +" " + z);

      if (j == 0)
      {
        base_x = x;
        base_y = y;

        cells.add(new Integer[] {0, 0, z});
      }
      else
      {
        cells.add(new Integer[] { base_x-x, base_y-y, z});
      }
    }
    Typology typology = new Typology(cells);
    typology_list.add(typology);
  }
  map.AddTypologies(typology_list);
}

