class Agent extends Ple_Agent {
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // FIELD
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  int participants;
  boolean is_active;

  Vec3D coming_loc;

  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTOR
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  Agent(PApplet _p5, Vec3D _loc) {
    super(_p5, _loc);
    this.participants = 500;
    this.is_active = true;

    this.coming_loc = null;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // METHODS
  /////////////////////////////////////////////////////////////////////////////////////////////////////

  //ADD VOLUME
  void addVolume(Map map) {
    int typ_count = map.typologies.size();
    //get topology
    Typology typology = map.typologies.get((int)(random(0,typ_count-1)));
    //create new cluster (cells) on agent position
    typology.createVolume(this.loc.x, this.loc.y, map);
  }  

  //ATTRACTION
  void attraction(float max_distance, float max_angle, float attraction_factor) {
    Map map = Map.getGrid();
    for (int i = 0; i < map.clusters.size(); i ++)
    {
      if (map.clusters.get(i).attraction)// && frameCount > 40) //Can starts without attraction
      {
        Vec3D target = new Vec3D(map.clusters.get(i).attractor.x, map.clusters.get(i).attractor.y, 0);
        Vec3D direction =  target.sub(this.loc);
        float orientation_angle = direction.angleBetween(this.vel, true);
        //      println("orientation is: " +orientation_angle);
        float distance = target.distanceTo(this.loc);
        if (distance < max_distance && orientation_angle < max_angle) //TODO variables (distance, angle) limits
        {
          this.seek(target, (max_distance - distance)*attraction_factor); //factor based on distance to target, coefficient smooths rotation
        }
      }
    }
  }

  //UPDATE NORMALIZED + INTERACT(test coming position)
  void interacting_update(boolean normalized) {
    // Update velocity
    vel.addSelf(acc);
    // Limit speed
    vel.limit(maxspeed);

    // Test coming position
    coming_loc = loc.add(vel);
    Map.getGrid().Interact(this, true);

    if (normalized)
      vel.normalize();

    loc.addSelf(vel);
    // Reset accelertion to 0 each cycle
    acc.clear();
  }


  //COLLISION - change direction
  void colision() {
    this.vel.x += random(-10, 10);
    this.vel.y += random(-10, 10);
    ///this.vel = this.vel.normalize();
    //TODO test new position until finds empty cell - correct direction (loc+vel to key -> vel)
  }

  //  NEVER USED 
  //  void occupy(){
  //    this.participants --;
  //    if(this.participants <= 0)
  //      this.is_active = false;
  //  }
}

