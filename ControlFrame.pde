public class ControlFrame extends PApplet {
  ControlP5 cp5;
  Object parent;
  int w, h;
 
  public void setup() {
    size(w, h);
    frameRate(25);
    cp5 = new ControlP5(this);
    //cp5.addSlider("ITE").plugTo(parent,"ITE").setRange(0, 15).setPosition(10, 70);
    cp5.addToggle("simergy").plugTo(parent,"simergy").setPosition(10, 10).setSize(10,10).setValue(false);
    cp5.addSlider("scatter").plugTo(parent,"scatter").setRange(1, 15).setPosition(10, 40);
    cp5.addSlider("decay").plugTo(parent,"decay").setRange(0.95, 1).setPosition(10, 60);
    cp5.addSlider("nSamples").plugTo(parent,"nSamples").setRange(5, 255).setPosition(10, 80);
    cp5.addSlider("att_distance").plugTo(parent,"att_distance").setRange(100, 500).setPosition(10, 100);
    cp5.addSlider("att_angle").plugTo(parent,"att_angle").setRange(1.4, 3.12).setPosition(10, 120);
    cp5.addSlider("att_factor").plugTo(parent,"att_factor").setRange(0.1, 1).setPosition(10, 140);
    
//    CheckBox checkbox =cp5.addCheckBox("checkBox")
//                .setPosition(10, 150)
//                .setColorForeground(color(120))
//                .setColorActive(color(255))
//                .setColorLabel(color(255))
//                .setSize(10, 10)
//                .setItemsPerRow(3)
//                .setSpacingColumn(30)
//                .setSpacingRow(20)
//                .addItem("simergy", 0)
//                .addItem("50", 50)
//                .addItem("100", 100)
//                .addItem("150", 150)
//                .addItem("200", 200)
//                .addItem("255", 255)
//                ;
//                checkbox.getItem(0).plugTo(parent,"simergy");
    //cp5.addSlider("pop", 50, 1000, pop, 10, 90, 200, 10);
    //cp5.addSlider("stigS", 0, 3, stigS, 10, 110, 200, 10);
  }

  public void draw() {
      background(100);
  }
  
  public ControlFrame(Object theParent, int theWidth, int theHeight) {
    parent = theParent;
    w = theWidth;
    h = theHeight;
  }
  
  public ControlP5 control() {
      return cp5;
  }

}
