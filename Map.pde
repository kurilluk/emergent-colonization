import java.util.Iterator;
import java.util.Set;
import java.util.Map;
import java.util.TreeMap;
//import java.io.*;
///////////////////////////////////////////////////////////////////////////////////////////////////////
static class Map
{
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // STATIC
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  private static Map instance;
  public static Map getGrid()
  {
    if (instance == null)
      instance = new Map();
    return instance;
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  long KeyGenerator(float x, float y)
  {
    int _x = (int)Math.floor(x*0.1); //0.1 coefficient depens on cell size!!!
    int _y = (int)Math.floor(y*0.1);
    return (((long)_x)<<32)+_y;
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // FIELD
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  TreeMap<Long, Cell> cell_grid;
  ArrayList<Cluster> clusters;
  ArrayList<Typology> typologies;
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTOR
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  private Map()
  {
    this.cell_grid = new TreeMap<Long, Cell>();
    this.clusters = new ArrayList<Cluster>();
    this.typologies = new ArrayList<Typology>();
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  // METHODS
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  void AddCell(Cell cell)
  {
    long cell_key = KeyGenerator( cell.x, cell.y);
    cell_grid.put(cell_key, cell); //HACK - hashMap > treeMap
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////  
  void AddClusters(ArrayList<Cluster> cluster_list)
  {
    this.clusters.addAll(cluster_list);
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////  
  void AddTypologies(ArrayList<Typology> typology_list)
  {
    this.typologies.addAll(typology_list);
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
//  void readClusters(String path, int cell_size)
//  {
//    // Read file
//    try {
//      // Open the file that is the first 
//      // command line parameter
//      FileInputStream fstream = new FileInputStream(path);
//      // Get the object of DataInputStream
//      DataInputStream in = new DataInputStream(fstream);
//      BufferedReader br = new BufferedReader(new InputStreamReader(in));
//      String txt_cluster;
//      int i = 0;
//      ArrayList<Cluster> cluster_list = new ArrayList<Cluster>();
//      //Read File Line By Line
//      while ( (txt_cluster = br.readLine ()) != null) {
//        String txt_cells[]= split (txt_cluster, " ");
//        //println(txt_cells[0]+" a "+txt_cells[1]);
//
//        //Get cluster cells
//        ArrayList<Cell> cluster_cells = new ArrayList<Cell>(txt_cells.length/3);
//        for (int j = 0; j < txt_cells.length-1; j += 3)
//        {
//          int x_cell = Integer.parseInt(txt_cells[j]);
//          int y_cell = -(Integer.parseInt(txt_cells[j+1]));
//          int z_cell = Integer.parseInt(txt_cells[j+2]);
//          //println(x +" "+ y +" " + z);
//
//          Cell cell =  new Cell(x_cell, y_cell, z_cell, cell_size, i);
//          cluster_cells.add(cell);
//          //Register cell in map
//          this.AddCell(KeyGenerator(x_cell, y_cell), cell);
//        }
//        //Create cluster and store it into a list
//        Cluster cluster = new Cluster(cluster_cells, i);
//        cluster_list.add(cluster);
//        i++;
//      }
//      //println("cells:" +map.map.size());
//      this.AddClusters(cluster_list);
//      //Close the input stream
//      in.close();
//    }
//    catch (Exception e) {//Catch exception if any
//      System.err.println("Error: " + e.getMessage());
//    }
//
//    //    String txt_clusters[] = loadStrings(path);  
//    //    if (txt_clusters == null) {
//    //      println(path + " file is missing!");
//    //      return;
//    //    }
//    //    // Get clusters from file
//    //    ArrayList<Cluster> cluster_list = new ArrayList<Cluster>(txt_clusters.length);
//    //    for (int i = 0; i< txt_clusters.length; i++) {
//    //      String txt_cells[]= split (txt_clusters[i], " ");
//    //      //println(txt_cells[0]+" a "+txt_cells[1]);
//    //
//    //      //Get cluster cells
//    //      ArrayList<Cell> cluster_cells = new ArrayList<Cell>(txt_cells.length/3, i);
//    //      for (int j = 0; j < txt_cells.length-1; j += 3)
//    //      {
//    //        int x = Integer.parseInt(cells[j]);
//    //        int y = -(Integer.parseInt(cells[j+1]));
//    //        int z = Integer.parseInt(cells[j+2]);
//    //        //println(x +" "+ y +" " + z);
//    //
//    //        Cell cell =  new Cell(x, y, z, cell_size, i);
//    //        cluster_cells.AddCell(cell);
//    //        //Register cell in map
//    //        this.AddCell(KeyGenerator(x, y), cell);
//    //      }
//    //      //Create cluster and store it into a list
//    //      Cluster cluster = new Cluster(cluster_cells, i);
//    //      cluster_list.add(cluster);
//    //    }
//    //    //println("cells:" +map.map.size());
//    //    this.AddClusters(cluster_list);
//  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  void Interact(Agent agent, boolean test_coming_loc)//float x, float y) //long agent_position)
  {
    long a_key;
    if (test_coming_loc)
      a_key = KeyGenerator(agent.coming_loc.x, agent.coming_loc.y);
    else
      a_key = KeyGenerator(agent.loc.x, agent.loc.y);

    //if cell exist on position (key ...)
    if (cell_grid.containsKey(a_key))
    {
      Cell cell = cell_grid.get(a_key);
      //interact with cluster
      clusters.get(cell.cluster_id).AgentInteraction(agent);
    }
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  boolean addNewCell(Cell cell)//HACK with addCell method
  {
    long a_key = KeyGenerator(cell.x, cell.y);
    //if cell already exists
    if (!cell_grid.containsKey(a_key)){ //TODO or if exist cell_grid.get(a_key).update(volume);
      this.cell_grid.put(a_key, cell); 
      return true;
    }
    else return false;
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  void Draw()
  {
    Iterator<Cell> i = cell_grid.values().iterator();
    while (i.hasNext ()) {
      i.next().Draw();
    }
    for (int c = 0; c < clusters.size(); c++)
    {
      clusters.get(c).Draw();
    }
  } 
  /////////////////////////////////////////////////////////////////////////////////////////////////////
}

