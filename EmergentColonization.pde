import plethora.core.*;
import toxi.geom.*;
import peasy.*;
/////////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FIELD
/////////////////////////////////////////////////////////////////////////////////////////////////////
PeasyCam cam;
Map map;
ArrayList<Agent> agents;
ArrayList<Ple_Agent> agents_ple;
int pop = 50;
boolean addVolume = false;
static final int sell_size = 10; //HACK global value for typology
boolean showPheromone = true; // show pheromone path
float att_distance = 150;
float att_angle = 1.4;
float att_factor = 0.1;
boolean simergy = false;

/////////////////////////////////////////////////////////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////////////////////////////////////////////////////////
void setup()
{
  size(1200, 900, P3D);
  //glLoadIdentity
  cam = new PeasyCam(this, 1000);
  cam.lookAt(0, 0, 0);
  //cam.setResetOnDoubleClick(false);
  //cam.setMinimumDistance(50);
  //cam.setMaximumDistance(500);

  //colorMode(HSB, 255);
  //initGUI();
  gui();

  map = Map.getGrid();
  ReadClusters("data/clusters.txt", sell_size, map); //FIXME processing IDE problems with static class Map to use readClusters as Map method
  ReadTypology("data/typologies.txt", map);
  //println("map consist of "+ map.clusters.size() +" clusters.");
  agents = new ArrayList<Agent>();
  for (int i = 0; i < pop; i++) {
    //create agents with the initial location as 0,0,0
    Agent agent = new Agent(this, new Vec3D());
    //generate a random initial velocity
    agent.setVelocity(new Vec3D (random(-1, 1), random(-1, 1), 0));
    //    //initialize the tail
    //    agent.initTail(5);
    //add the agents to the list
    agents.add(agent);
  }
  agents_ple = (ArrayList<Ple_Agent>) ((ArrayList<?>) agents);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

void draw()
{
  img.loadPixels();
  background(115);
  map.Draw();
  for (Agent agent : agents) {
    if (!agent.is_active)
      continue;

    //wander: inputs: circleSize, distance, variation in radians
    agent.wander2D(5, 0, PI);
    //separation
    agent.separationCall(agents_ple, 10F, 10F);
    //define the boundries of the sagentce as bounce
    agent.bounceSpace(1500, 1500, 0); //TODO get boundaries from map class (agents don't return)
    //update the tail info every frame (1)
    //    agent.updateTail(1);
    //    //display the tail interpolating 2 sets of values:
    //    //R,G,B,ALPHA,SIZE - R,G,B,ALPHA,SIZE
    //    agent.displayTailPoints(0, 0, 0, 0, 1, 0, 0, 0, 255, 1);
    //    //set the max speed of movement:
    //    //agent.setMaxspeed(2);
    //    //agent.setMaxforce(0.05);

    //attraction
    agent.attraction(att_distance, att_angle, att_factor);


    //STIGMERGY  
    if (simergy) {   
      agent.vel.normalize(); //unitalize actual velocity vector for other calculation
      Vec3D futLoc = agent.futureLoc(scatter); //scatter as distance of future localization
      Vec3D bestLoc = new Vec3D(); //default position

      //FIND MAX VALUE AROUND
      float val = -1; //default min value
      for (int i = 0; i < nSamples; i++) {    
        Vec3D v = futLoc.add(new Vec3D(random(-scatter, scatter), random(-scatter, scatter), 0));//scatter = distance radius
        float sampleVal = field.readValue(v);
        if (sampleVal > val) {
          val = sampleVal;
          bestLoc = v;
        }
      }

      Vec3D stigVec = bestLoc.sub(agent.loc).normalize().scale(stigS);//stigmergic vector with scale
      agent.vel.addSelf(stigVec);//orient agent by stigmegic vector (add velocity)
      agent.vel.normalize();//to do a constant speed

      field.addValue(agent.loc, 75);//spread pheromone
      //agent.update(); //move agent to new position
    }

    //update agents location based on agentst calculations
    agent.interacting_update(true); //normalized velocity vector (sum of vel + acc)(no acceleration)

    //Add new volume from stored topologies
    if (addVolume) {
      agent.addVolume(map);
      //addVolume = false;
    }

    //Display the location of the agent with a point
    strokeWeight(3);//agent.participants*0.1);
    //stroke(255);
    agent.displayPoint();
    //    //Display the direction of the agent with a line
    //    strokeWeight(1);
    //    stroke(100, 90);
    //    agent.displayDir(agent.vel.magnitude()*3);
  }
  if (decay<1)field.decay(decay);
  img.updatePixels();
  if (showPheromone)field.drawField();
  addVolume = false;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void keyPressed() {
  if (key == 't' || key == 'T') {
    addVolume = true;
  }
  if (key=='p' || key=='P') showPheromone = !showPheromone;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
//int id = 308;
//void keyPressed() {
// if (key == 'e' || k == 'E') {
//    id = (int)random(p.clusters.size());
//    Cluster c = map.csters.get(id);
//    c.Empty();
//  }
//  if (key == 'f|| k == 'F') {
//    id = (int)random(p.clusters.size());
//    Cluster c = map.csters.get(id);
//    c.Fill();
//  }
// if (key == 'a|| key == 'A') {
//    id = (int)random(map.clusters.size());
//    Cluster c = map.clusters.get(id);
//    //c.AgentInteraction(18);
//  }
/////////////////////////////////////////////////////////////////////////////////////////////////////
//}

